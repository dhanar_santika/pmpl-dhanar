from django.urls import reverse
from django.test import TestCase
from django.http import HttpRequest
from home.views import index

# Create your tests here.
class HomePageTest(TestCase):
	def test_root_url_resolves_to_index_view(self):
		found = reverse("index")
		self.assertEqual(found, "/")

	def test_index_return_correct_html(self):
		request = HttpRequest()
		response = index(request)
		self.assertTrue(response.content.startswith(b'<html>'))
		self.assertIn(b'<title>Homepage</title>', response.content)
		self.assertIn(b'Made Wira Dhanar Santika', response.content)
		self.assertTrue(response.content.endswith(b'</html>'))

	def test_index_has_link_to_gitlab_repo(self):
		request = HttpRequest()
		response = index(request)
		self.assertIn(b'https://gitlab.cs.ui.ac.id/pmpl/practice-collection/2019/1606880996-practice', response.content)

	def test_index_shows_contact(self):
		request = HttpRequest()
		response = index(request)
		self.assertIn(b'made.wira61@ui.ac.id', response.content)
