from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def index(request):
	html = """<html>
		<head>
			<title>Homepage</title>
		</head>
		<body>
			<p>Hello, my name is Made Wira Dhanar Santika</p>
			<p>My gitlab repo : <a href="https://gitlab.cs.ui.ac.id/pmpl/practice-collection/2019/1606880996-practice">url</a></p>
			<p>Feel free to contact me : made.wira61@ui.ac.id</p>
			<p><a id="todo_link" href="todo/">To-Do</a></p>
		</body>
	</html>"""
	return HttpResponse(html)