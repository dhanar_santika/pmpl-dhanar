from django.test import TestCase
from django.urls import reverse
from django.http import HttpRequest
from django.template.loader import render_to_string
from todo.views import todo
from todo.models import Item

# Create your tests here.
class TodoPageTests(TestCase):
    def test_todo_url_resolves_to_todo_view(self):
        found = reverse("todo")
        self.assertEqual(found, "/todo/")

    def test_todo_return_correct_html(self):
        request = HttpRequest()
        response = todo(request)
        self.assertTrue(response.content.startswith(b'\n\n\n<html>\n'))
        self.assertIn(b'<title>To-Do Lists</title>', response.content)
        self.assertTrue(response.content.endswith(b'</html>'))

    def test_todo_page_can_save_a_POST_request(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['item_text'] = 'A new list item'

        response = todo(request)
        self.assertEqual(Item.objects.count(), 1)
        new_item = Item.objects.first()
        self.assertEqual(new_item.text, 'A new list item')

    def test_todo_page_only_saves_items_when_necessary(self):
        request = HttpRequest()
        todo(request)
        self.assertEqual(Item.objects.count(), 0)

    def test_todo_page_redirect_after_a_POST_request(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['item_text'] = 'A new list item'
        response = todo(request)
        
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/todo/')

    def test_todo_page_displays_all_list_items(self):
        Item.objects.create(text='itemey 1')
        Item.objects.create(text='itemey 2')
        request = HttpRequest()
        response = todo(request)
        
        self.assertIn('itemey 1', response.content.decode())
        self.assertIn('itemey 2', response.content.decode())

    def test_todo_page_get_custom_comment_if_items_is_empty(self):
        request = HttpRequest()
        response = todo(request)

        self.assertEqual(0, Item.objects.count())
        self.assertIn("<p class=\"comment\">yey, waktunya berlibur</p>", response.content.decode())

    def test_todo_page_get_custom_comment_if_items_is_1(self):
        Item.objects.create(text='dead 1')
        request = HttpRequest()
        response = todo(request)

        self.assertEqual(1, Item.objects.count())
        self.assertIn("<p class=\"comment\">sibuk tapi santai</p>", response.content.decode())

    def test_todo_page_get_custom_comment_if_items_is_too_many(self):
        for i in range(100):
            Item.objects.create(text='dead {}'.format(i))
        
        request = HttpRequest()
        response = todo(request)

        self.assertEqual(100, Item.objects.count())
        self.assertIn("<p class=\"comment\">oh tidak</p>", response.content.decode())

class ItemModelTest(TestCase):
    def test_saving_and_retrieving_items(self):
        first_item = Item()
        first_item.text = 'The first (ever) list item'
        first_item.save()
        second_item = Item()
        second_item.text = 'Item the second'
        second_item.save()
        saved_items = Item.objects.all()
        self.assertEqual(saved_items.count(), 2)
        first_saved_item = saved_items[0]
        second_saved_item = saved_items[1]
        self.assertEqual(first_saved_item.text, 'The first (ever) list item')
        self.assertEqual(second_saved_item.text, 'Item the second')