from django.shortcuts import render, redirect
from django.http import HttpResponse
from todo.models import Item

# Create your views here.
def todo(request):
    if request.method == 'POST':
        Item.objects.create(text=request.POST['item_text'])
        return redirect('/todo/')

    items = Item.objects.all()
    comment = get_custom_comment(items)
    return render(request, 'todo.html', {'items': items, 'comment': comment})

def get_custom_comment(items):
    if(items.count() == 0):
        return "yey, waktunya berlibur"
    elif(1 <= items.count() < 5):
        return "sibuk tapi santai"
    elif(items.count() >= 5):
        return "oh tidak"